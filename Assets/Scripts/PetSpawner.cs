using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PetSpawner : MonoBehaviour
{
    public GameObject pet;

    private void Start()
    {
        StartCoroutine(SpawnPet());
    }

    private IEnumerator SpawnPet()
    {
        yield return new WaitForSeconds(Random.Range(0.5f, 5f));
        Instantiate(pet, Vector3.zero, Quaternion.identity);
        StartCoroutine(SpawnPet());
    }
}
