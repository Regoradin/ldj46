using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Person : MonoBehaviour
{
    public LineRenderer line;
    public Vector3 position;

    public float distance;
    public float distanceSpeed;
    public float distanceAccel;

    protected float minDistance;
    protected float offsetAmp;
    protected float randomNumber;

    public bool isYou;
    public int maxAge;
    public bool isEnded = false;
    public Text ageText;

    protected List<Vector3> linePositions;

    public float friendzone;
    public float friendTimerMax;
    private float friendTimer;

    private float smoothVelocity;
    
    protected void Start()
    {
        line = GetComponent<LineRenderer>();
        if (linePositions == null)
        {
            linePositions = new List<Vector3>();
        }
        randomNumber = Random.Range(0, Mathf.PI * 2);

        minDistance = Random.Range(0.8f, 1.2f);
        offsetAmp = Random.Range(0.1f, 0.3f);

        friendTimer = friendTimerMax;

        if (!isYou)
        {
            linePositions.Add(new Vector3(transform.position.x, -distance, transform.position.z));
            line.positionCount = linePositions.Count;
            line.SetPositions(linePositions.ToArray());
        }

        if(!isYou && this as Pet == null)
        {
            //distanceAccel = Random.Range(-0.05f, 0.1f);
            distanceSpeed = Random.Range(-0.05f, 0.1f);
        }
    }

    protected virtual void Update()
    {
        position = line.GetPosition(line.positionCount-1);
        
        UpdateDistance();
        LimitDistance();

        Vector3 newPosition = new Vector3(position.x + Time.deltaTime, -distance, position.z);
        linePositions.Add(newPosition);
        line.positionCount = linePositions.Count;
        line.SetPositions(linePositions.ToArray());

        if (distance <= friendzone)
        {
            friendTimer -= Time.deltaTime;
            if (friendTimer <= 0)
            {
                BecomeFriend();
            }
        }
        else
        {
            friendTimer = friendTimerMax;
        }

        if (isYou)
        {
            ageText.text = Mathf.Floor(Mathf.Min(position.x, maxAge)).ToString();
        }
        if (isYou && position.x >= maxAge && !isEnded)
        {
            isEnded = true;
            ChangeColor(new Color(0, 0, 0, 0));
        }
        if (isYou && position.x >= maxAge + 5)
        {
            Destroy(this);
        }
    }

    protected void UpdateDistance()
    {
        if (!isYou)
        {
            distanceSpeed += distanceAccel * Time.deltaTime;
            distance += distanceSpeed * Time.deltaTime;

            //Damping
            distanceAccel -= Time.deltaTime * Mathf.Sign(distanceAccel);
            //distanceSpeed /= 1.2f;
            if (Mathf.Abs(distanceSpeed) > 5)
            {
                distanceSpeed /= 2;
            }
        }
    }

    protected void LimitDistance()
    {
        if (!isYou)
        {
            if (distance <= minDistance)
            {
                distance =  minDistance - (offsetAmp * Mathf.Sin(Time.time + randomNumber) + offsetAmp);
            }
            if (distance <= 0)
            {
                distance = 0;
            }
        }
    }
        
    public virtual void TakeEvent(float strength = 1, bool fromAction = false)
    {
        distanceSpeed *= strength;
    }

    public virtual void TakeTrend(float strength = 1, bool fromAction = false)
    {
        distanceAccel += strength;
    }


    public void Interact()
    {

    }

    public void CutLine()
    {
        GameObject obj = new GameObject();
        LineRenderer newLine = obj.AddComponent(typeof(LineRenderer)) as LineRenderer;
        newLine.positionCount = linePositions.Count;
        newLine.SetPositions(linePositions.ToArray());
        newLine.startWidth = line.startWidth;
        newLine.numCapVertices = line.numCapVertices;
        newLine.material = new Material(line.material);
        newLine.sortingOrder = line.sortingOrder;

        linePositions = new List<Vector3>();
        linePositions.Add(position);
    }

    public void ChangeColor(Color color)
    {
        CutLine();
        GetComponent<Renderer>().material.SetColor("_BaseColor", color);
    }

    public void ChangeColor()
    {
        ChangeColor(new Color(Random.Range(0.1f, 1f), Random.Range(0.1f, 1f), Random.Range(0.1f, 1f)));
    }

    public void BecomeFriend()
    {
        if (!isYou && !(this is Friend))
        {
            Friend friend = gameObject.AddComponent(typeof(Friend)) as Friend;
            friend.CopyValues(line, position, distance, distanceSpeed, distanceAccel, minDistance, offsetAmp, randomNumber, linePositions);

            AudioSource audio = GetComponent<AudioSource>();
            audio.clip = AudioPlayer.Instance.audios[Random.Range(0, AudioPlayer.Instance.audios.Count)];
            audio.loop = true;
            audio.Play();

            PersonManager.Instance.AddPerson(friend);
            DestroyImmediate(this);
        }
    }
}
