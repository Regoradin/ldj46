using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Milestone : MonoBehaviour
{
    public Person you;
    public PersonSpawner spawner;
    public ActionManager actionManager;
    public bool visible = true;
    public Material mat;

    public float point;
    private bool crossed = false;

    [Header("Things that happen")]
    public int peopleCount;
    public Vector2 distanceRange;
    public int bumpCount;
    public int groupCount;

    private void Start()
    {
        if (visible)
        {
            GameObject lineObj = new GameObject();
            LineRenderer line = lineObj.AddComponent(typeof(LineRenderer)) as LineRenderer;
            line.material = mat;
            line.startWidth = 0.2f;
            Vector3[] points = new Vector3[2];
            points[0] = new Vector3 (point, 1000, 0);
            points[1] = new Vector3 (point, -1000, 0);
            line.positionCount = 2;
            line.SetPositions(points);
        }
    }

    private void Update()
    {
        if (!crossed && you.position.x >= point)
        {
            crossed = true;
            for (int i = 0; i < peopleCount; i++)
            {
                spawner.SpawnPerson(Random.Range(distanceRange.x, distanceRange.y));
            }
            actionManager.AddBump(bumpCount);
            actionManager.AddGroup(groupCount);
        }
    }
}
