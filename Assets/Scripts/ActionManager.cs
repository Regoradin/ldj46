using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ActionManager : MonoBehaviour
{
    public Transform canvas;
    public Person you;
    public AudioSource actionAudio;
    
    public GameObject bumpAction;
    public Transform bumpSpot;
    public Text bumpText;
    public int startingBumpCount;
    private int bumpCount;

    public GameObject groupAction;
    public Transform groupSpot;
    public Text groupText;
    public int startingGroupCount;
    private int groupCount;


    private void Start()
    {
        AddBump(startingBumpCount);
        AddGroup(startingGroupCount);
    }

    public void AddBump(int count)
    {
        for(int i = 0; i < count; i++)
        {
            BumpAction bump = Instantiate(bumpAction, bumpSpot).GetComponent<BumpAction>();
            bump.actionManager = this;
            bump.you = you;
            bump.actionAudio = actionAudio;
            bump.transform.SetParent(canvas);
            bumpCount++;
            bumpText.text = bumpCount.ToString();
        }
    }
    public void AddGroup(int count)
    {
        for(int i = 0; i < count; i++)
        {
            GroupAction group = Instantiate(groupAction, groupSpot).GetComponent<GroupAction>();
            group.actionManager = this;
            group.you = you;
            group.actionAudio = actionAudio;
            group.transform.SetParent(canvas);
            groupCount++;
            groupText.text = groupCount.ToString();
        }
    }

    public void RemoveBump()
    {
        bumpCount--;
        bumpText.text = bumpCount > 0 ? bumpCount.ToString() : "";
    }
    public void RemoveGroup()
    {
        groupCount--;
        groupText.text = groupCount > 0 ? groupCount.ToString() : "";
    }

}
