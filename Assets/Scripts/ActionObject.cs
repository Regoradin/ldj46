using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionObject : MonoBehaviour
{
    private bool isHeld = false;

    private RectTransform trans;
    private BoxCollider2D coll;
    public ActionManager actionManager;
    public Person you;

    private Vector3 oldPosition;
    public AudioSource actionAudio;

    private void Start()
    {
        trans = (RectTransform)transform;
        coll = GetComponent<BoxCollider2D>();
    }

    protected void Update()
    {
        if (isHeld)
        {
            Vector3 boundedMousePosition = new Vector3(
                                                       Mathf.Clamp(Input.mousePosition.x, 0, Screen.width),
                                                       Mathf.Clamp(Input.mousePosition.y, 0, Screen.height),
                                                       0);
            
            trans.anchoredPosition = boundedMousePosition;
        }
        if (you.isEnded)
        {
            Remove();
        }

        coll.size = new Vector2(trans.rect.width, trans.rect.height);
    }
    
    private void OnMouseDown()
    {
        isHeld = true;
        oldPosition = transform.position;
    }

    private void OnMouseUp()
    {
        isHeld = false;
        Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));

        Person target = PersonManager.Instance.SelectNearestPerson(point, 2f);
        List<Person> targets = PersonManager.Instance.SelectPeopleNearPoint(point, 1f);

        bool valid = false;
        if (target != null)
        {
            Activate(target);
            valid = true;
        }
        if (targets.Count != 0)
        {
            Activate(targets);
            valid = true;
        }

        if (valid)
        {
            Remove();
        }
        else
        {
            Debug.Log("reverting position");
            transform.position = oldPosition;
        }

    }

    protected virtual  void Activate(Person target)
    {
    }

    protected virtual void Activate(List<Person> targets)
    {
    }

    protected virtual void Remove()
    {
        Destroy(gameObject);
    }

}
