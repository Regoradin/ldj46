using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PersonSpawner : MonoBehaviour
{
    public GameObject person;
    public GameObject pet;
    public float interval;
    public Person you;
    private int nextOrder;

    private void Start()
    {
//        StartCoroutine(SpawnPersonLoop());
        StartCoroutine(SpawnPet(Random.Range(0.5f, 5f)));
        nextOrder = 1;
    }

    private IEnumerator SpawnPersonLoop()
    {
        yield return new WaitForSeconds(interval);
        SpawnPerson(Random.Range(3f, 7f));
        StartCoroutine(SpawnPersonLoop());
    }

    public Person SpawnPerson(float distance)
    {
        Person newPerson = Instantiate(person, new Vector3(you.position.x, 0f, 0f), Quaternion.identity, this.transform).GetComponent<Person>();
        newPerson.distance = distance;
        newPerson.GetComponent<LineRenderer>().sortingOrder = nextOrder;
        nextOrder++;

        PersonManager.Instance.AddPerson(newPerson);

        return newPerson;
    }

    public void SpawnPerson()
    {
        Person newPerson = Instantiate(person, new Vector3(you.position.x, 0f, 0f), Quaternion.identity, this.transform).GetComponent<Person>();
        newPerson.GetComponent<LineRenderer>().sortingOrder = nextOrder;
        nextOrder++;

        PersonManager.Instance.AddPerson(newPerson);

    }

    public IEnumerator SpawnPet(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Pet newPet = Instantiate(pet, new Vector3(you.position.x, 0f, 0f), Quaternion.identity).GetComponent<Pet>();
        newPet.spawner = this;
        newPet.GetComponent<LineRenderer>().sortingOrder = nextOrder;
        nextOrder++;
    }

}
