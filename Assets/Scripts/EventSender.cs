using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventSender : MonoBehaviour
{
    public float minWait;   //maybe take into account total number of people, to keep the density up.
    public float maxWait;

    [Range(0, 1)]
    public float eventChance;
    [Range(0, 1)]
    public float trendChance;
    
    private void Start()
    {
        StartCoroutine(SendEvent());
    }

    private IEnumerator SendEvent()
    {
        Person person = PersonManager.Instance.GetRandomPerson();
        if (person != null)
        {
            if (Random.Range(0, 1f) < eventChance)
            {
                if (Random.Range(0, 1f) <0.3)
                {
                    //good event
                    person.TakeEvent(Random.Range(0.7f, 2.5f));
                }
                else
                {
                    //bad event
                    person.TakeEvent(Random.Range(-2.5f, .7f));
                }
            }
            if (Random.Range(0, 1f) < trendChance)
            {
                person.TakeTrend(Random.Range(-0.1f, 0.1f));
            }
        }
        
        yield return new WaitForSeconds(Random.Range(minWait, maxWait));
        StartCoroutine(SendEvent());
    }
}
