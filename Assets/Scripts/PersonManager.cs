using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PersonManager : Singleton<PersonManager>
{
    private List<Person> people;
    
    public void AddPerson(Person person)
    {
        if (people == null)
        {
            people = new List<Person>();
        }
        people.Add(person);
    }
    
    public Person GetRandomPerson()
    {
        if (people == null)
        {
            return null;
        }
        return people[Random.Range(0, people.Count)];
    }

    public List<Person> SelectPeopleNearPoint(Vector3 point, float radius)
    {
        List<Person> result = new List<Person>();
        if (people == null || people.Count == 0)
        {
            return result;
        }
        foreach(Person person in people)
        {
            if (Vector3.Distance(person.position, point) <= radius)
            {
                result.Add(person);
            }
        }
        return result;
    }

    public Person SelectNearestPerson(Vector3 point, float maxRadius = Mathf.Infinity)
    {
        if (people == null || people.Count == 0)
        {
            return null;
        }
        Person bestPerson = people[0];
        float bestDistance = Mathf.Infinity;
        foreach(Person person in people)
        {
            float distance = Vector3.Distance(person.position, point);
            if (distance < bestDistance)
            {
                bestPerson = person;
                bestDistance = distance;
            }
        }

        if(bestDistance <= maxRadius)
        {
            return bestPerson;            
        }
        else
        {
            return null;
        }

    }
}
