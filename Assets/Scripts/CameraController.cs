﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public LineRenderer trackedLine;
    public float offset;
    public float shift;
    public Vector3 start;

    private void Start()
    {
        start = trackedLine.GetPosition(0);
    }
    
    private void Update()
    {
        Vector3 end = trackedLine.GetPosition(trackedLine.positionCount -1);
        
        float lineLength = end.x - start.x + shift;

        float horizontalPosition = start.x + ((lineLength / 2));
        float verticalPosition = -(lineLength / (4 * Mathf.Tan(Camera.main.fieldOfView * Mathf.Deg2Rad / 2))) - offset;

        transform.position = new Vector3(horizontalPosition, transform.position.y, verticalPosition);
    }
}
