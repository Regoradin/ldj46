using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroupAction : ActionObject
{
    protected override void Activate(List<Person> targets)
    {
        foreach(Person person in targets)
        {
            person.TakeTrend(Random.Range(-1f, 0.5f), true);
        }
        actionAudio.Play();
    }

    protected override void Remove()
    {
        actionManager.RemoveGroup();
        Destroy(gameObject);
    }
}
