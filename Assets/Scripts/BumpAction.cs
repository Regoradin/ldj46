using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BumpAction : ActionObject
{
    protected override void Activate(Person target)
    {
        target.TakeTrend(Random.Range(-1f, -2f), true);
        actionAudio.Play();
    }

    protected new void Update()
    {
        base.Update();
    }
    
    protected override void Remove()
    {
        actionManager.RemoveBump();
        Destroy(gameObject);
    }
}
