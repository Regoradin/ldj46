using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Friend : Person
{
    private Color originalColor;
    public AudioSource audio;
    
    private new void Start()
    {
        line = GetComponent<LineRenderer>();
        ChangeColor();
        audio = GetComponent<AudioSource>();
        originalColor = line.material.GetColor("_BaseColor");
        StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
        Color color = line.material.GetColor("_BaseColor");
        //color += new Color(1f/256f,1f/256f,1f/256f);
        Color background = new Color (1f, 0.9921569f, 0.8156863f);
        color = color + ((background - color) / 30);
        ChangeColor(color);

        FadeAudio();
        
        yield return new WaitForSeconds(0.3f);
        if (color != background)//Color.black)
        {
            StartCoroutine(Fade());
        }
    }

    private void RefreshColor()
    {
        ChangeColor(originalColor);
        audio.volume = 1;
    }

    private void FadeAudio()
    {
        audio.volume *= 0.7f;
    }
    
    protected override void Update()
    {
        base.Update();
    }

    public override void TakeEvent(float strength, bool fromAction = false)
    {
        if (fromAction)
        {
            RefreshColor();
        }
        base.TakeEvent(strength * 0.7f);
    }

    public override void TakeTrend(float strength, bool fromAction = false)
    {
        if (fromAction)
        {
            RefreshColor();            
        }
        base.TakeTrend(strength * 0.7f);
    }

    public void CopyValues(LineRenderer line, Vector3 position, float distance, float distanceSpeed, float distanceAccel, float minDistance, float offsetAmp, float randomNumber, List<Vector3> linePositions)
    {
        this.line = line;
        this.position = position;
        this.distance = distance;
        this.distanceSpeed = distanceSpeed;
        this.minDistance = minDistance;
        this.offsetAmp = offsetAmp;
        this.randomNumber = randomNumber;
        this.linePositions = linePositions;
    }
}
