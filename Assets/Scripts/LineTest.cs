﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LineTest : MonoBehaviour
{
    private LineRenderer line;
    private Vector3 position;
    public Vector3 speed;
    [Range(-0.1f, 0.1f)]
    public float heightAccel;
    [Range(-1f, 1f)]
    public float heightSpeed;
    public float height;

    public float maxHeight;
    public float maxHeightVariance;
    private float timeOffset;

    [Header("Event Stuff")]
    public Vector2 eventStrength;
    public Vector2 trendStrength;
    
    private List<Vector3> linePositions;
    
    private void Start()
    {
        line = GetComponent<LineRenderer>();
        linePositions = new List<Vector3>();
        position = transform.position;
        height = position.y;

        timeOffset = Random.Range(0f, 3f);
    }

    private void Update()
    {
        heightSpeed += heightAccel * Time.deltaTime;
        height += heightSpeed * Time.deltaTime;

        position += speed * Time.deltaTime;

        float thisHeight = Mathf.Min(height, maxHeight) + (Mathf.Sin(timeOffset + Time.time) * maxHeightVariance);;
        
        position = new Vector3(position.x, thisHeight, position.z);

        linePositions.Add(position);

        line.positionCount = linePositions.Count;
        line.SetPositions(linePositions.ToArray());
    }

    public void TakeEvent(float strength)
    {
        heightSpeed *= Random.Range(eventStrength.x, eventStrength.y);
    }

    public void TakeTrend(float strength)
    {
        heightAccel += Random.Range(trendStrength.x, trendStrength.y);
    }
}
