using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pet : Person
{
    public float lifetimeRemaining;
    public float minLifetime;
    public float maxLifetime;

    public PersonSpawner spawner;
    private bool dead;
    
    private new void Start()
    {
        line = GetComponent<LineRenderer>();
        linePositions = new List<Vector3>();
        randomNumber = Random.Range(0, Mathf.PI * 2);

        distance = Random.Range(0.2f, 0.7f);
        linePositions.Add(new Vector3(transform.position.x, -distance, transform.position.z));
        line.positionCount = linePositions.Count;
        line.SetPositions(linePositions.ToArray());

        lifetimeRemaining = Random.Range(minLifetime, maxLifetime);
        dead = false;
    }
    
    private new void Update()
    {
        if (lifetimeRemaining >= 0)
        {
            base.Update();
        }
        lifetimeRemaining -= Time.deltaTime;
        if (lifetimeRemaining <= 0.6f && !dead)
        {
            StartCoroutine(spawner.SpawnPet(Random.Range(0.5f, 15f)));
            dead = true;
        }
    }

}
